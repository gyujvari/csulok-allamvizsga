import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/formpage',
        name: 'FormPage',
        component: () => import('@/components/FormPage.vue'),
        meta: { requiresAuth: true },
    },
    {
        path: '/',
        name: 'LoginPage',
        component: () => import('@/components/Login.vue'),
    },
    {
        path: '/selectsheet',
        name: 'SelectSheet',
        component: () => import('@/components/SelectSheetpage.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        let isLoggedIn = sessionStorage.getItem('isLoggedIn');
        if (isLoggedIn === 'false' || isLoggedIn === null) {
            next({
                name: 'LoginPage',
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;
