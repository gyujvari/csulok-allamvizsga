import Vue from 'vue';
import Vuex from 'vuex';
import { RestClient } from '@/modules/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        sheets: null,
        isLoggedIn: false,
        accessToken: null,
        restClientModule: null,
    },
    getters: {
        getSheets(state) {
            return state.sheets;
        },
        getIsLoggedIn(state) {
            return sessionStorage.getItem('isLoggedIn') !== null ? sessionStorage.getItem('isLoggedIn') : state.isLoggedIn;
        },
        getUserAccessToken(state) {
            return sessionStorage.accessToken ? sessionStorage.accessToken : state.accessToken;
        },
    },
    mutations: {
        setSheets(state, sheets) {
            state.sheets = sheets;
        },
        setLoggedIn(state, payload) {
            state.isLoggedIn = payload;
        },
        setAccessToken(state, payload) {
            state.accessToken = payload;
        },
    },
    actions: {
        fetchUser({ dispatch }, payload) {
            return dispatch('getRestClientModule').then((client) => {
                return client.post('/api/login', payload).then((result) => {
                    dispatch('setLoggedInState', true);
                    console.log('resultLogin', result);
                    dispatch('setAccessTokenState', result.data.access_token);
                });
            });
        },
        fetchSheets({ commit, dispatch }) {
            console.log('inside fachsheets');
            return dispatch('getRestClientModule')
                .then((sheets) => {
                    console.log('inside rest client');
                    return sheets
                        .get('/api/google_sheets')
                        .then((result) => {
                            console.log('Get Sheets');
                            commit('setSheets', result.data);
                            console.log('Setter Sheet', result.data);
                            return Promise.resolve();
                        })
                        .catch((err) => {
                            console.log(err);
                        });
                })
                .catch(() => {
                    return Promise.reject();
                });
        },

        setAccessTokenState({ commit }, payload) {
            sessionStorage.accessToken = payload;
            commit('setAccessToken');
        },

        sendFormData({ dispatch }, payload) {
            return dispatch('getRestClientModule')
                .then((form) => {
                    return form.post('/api/liquidity', payload).then(() => {
                        return Promise.resolve();
                    });
                })
                .catch((error) => {
                    console.log('error', error);

                    return Promise.reject();
                });
        },
        getRestClientModule(context) {
            if (!context.state.restClientModule) {
                context.state.restClientModule = new RestClient('', context);
            }
            return context.state.restClientModule;
        },
    },
});
