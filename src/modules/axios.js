import axios from 'axios';

axios.defaults.withCredentials = true;


class RestClient {
    constructor(endpoint, context) {
        this.context = context;

        this.HTTP = axios.create({
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
        });

        this.BASE_URL = 'http://mha.fun';

        this.endpoint = endpoint;
    }

    get(id) {
        return this.HTTP.get(`${this.BASE_URL}${this.endpoint}${id}`)
                .then((res) => this.httpCallSuccess(res, id))
                .catch((rej) => this.httpCallError(rej, id))
    }
    post(id, data) {

            return this.HTTP.post(`${this.BASE_URL}${this.endpoint}${id}`, data)
                .then((res) => {
                    return Promise.resolve(this.httpCallSuccess(res, id));
                })
                .catch((rej) => {
                    return Promise.reject(this.httpCallError(rej, id));
                });

    }

    httpCallSuccess(res) {
        return res;
    }
    httpCallError(error) {
        return error;
    }

    getAccessToken(){
        return this.context.getters['getIsLoggedIn']
    }
}

export { RestClient };
